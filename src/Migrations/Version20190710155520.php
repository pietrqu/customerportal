<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190710155520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE trip_passengers (id INT AUTO_INCREMENT NOT NULL, tp_user_trips_link_id INT DEFAULT NULL, tp_user_passengers_link_id INT DEFAULT NULL, INDEX IDX_1645559C35CA6297 (tp_user_trips_link_id), INDEX IDX_1645559CEEB67442 (tp_user_passengers_link_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trips (id INT AUTO_INCREMENT NOT NULL, tr_name VARCHAR(255) NOT NULL, tr_location_from VARCHAR(255) NOT NULL, tr_location_to VARCHAR(255) NOT NULL, tr_departure DATETIME NOT NULL, tr_arrival DATETIME NOT NULL, is_active_trip TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_passengers (id INT AUTO_INCREMENT NOT NULL, user_id_link_id INT DEFAULT NULL, p_title VARCHAR(255) NOT NULL, p_name VARCHAR(255) NOT NULL, p_surname VARCHAR(255) NOT NULL, passport VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_44304866D45E6DED (user_id_link_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_trips (id INT AUTO_INCREMENT NOT NULL, ut_trips_link_id INT DEFAULT NULL, tr_owner_id INT NOT NULL, INDEX IDX_48B18B82AE3702D7 (ut_trips_link_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trip_passengers ADD CONSTRAINT FK_1645559C35CA6297 FOREIGN KEY (tp_user_trips_link_id) REFERENCES user_trips (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE trip_passengers ADD CONSTRAINT FK_1645559CEEB67442 FOREIGN KEY (tp_user_passengers_link_id) REFERENCES user_passengers (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_passengers ADD CONSTRAINT FK_44304866D45E6DED FOREIGN KEY (user_id_link_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_trips ADD CONSTRAINT FK_48B18B82AE3702D7 FOREIGN KEY (ut_trips_link_id) REFERENCES trips (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user ADD addr_country VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_trips DROP FOREIGN KEY FK_48B18B82AE3702D7');
        $this->addSql('ALTER TABLE trip_passengers DROP FOREIGN KEY FK_1645559CEEB67442');
        $this->addSql('ALTER TABLE trip_passengers DROP FOREIGN KEY FK_1645559C35CA6297');
        $this->addSql('DROP TABLE trip_passengers');
        $this->addSql('DROP TABLE trips');
        $this->addSql('DROP TABLE user_passengers');
        $this->addSql('DROP TABLE user_trips');
        $this->addSql('ALTER TABLE user DROP addr_country');
    }
}
