<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190704164135 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD username VARCHAR(255) NOT NULL, ADD last_login DATETIME DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD addr_street VARCHAR(255) DEFAULT NULL, ADD addr_number VARCHAR(255) DEFAULT NULL, ADD addr_post_code VARCHAR(255) DEFAULT NULL, ADD addr_city VARCHAR(255) DEFAULT NULL, CHANGE email email VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677 ON user');
        $this->addSql('ALTER TABLE user DROP username, DROP last_login, DROP created_at, DROP addr_street, DROP addr_number, DROP addr_post_code, DROP addr_city, CHANGE email email VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
