<?php

namespace App\Form;

use App\Entity\Trips;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TripsType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			//also can be as entity list (really entited not needed here)
			
			->add(
				'trName',
				TextType::class,
				[
					'label' => false,
					'attr'  => ['placeholder' => 'the name of the trip'],
				]
			)
			->add(
				'trLocationFrom',
				TextType::class,
				[
					'label' => false,
					'attr'  => ['placeholder' => 'Departure airport'],
				]
			)
			->add(
				'trLocationTo',
				TextType::class,
				[
					'label' => false,
					'attr'  => ['placeholder' => 'Destination airport'],
				]
			)
			->add(
				'trDeparture',
				DateTimeType::class,
				[
					'label'  => 'Departure Date',
					'html5'  => true,
//					'widget' => 'single_text',
					'attr' => [
						'placeholder' => 'Arrival date',
						'class'       => 'boldText',
					],
				]

			)
			->add(
				'trArrival',
				DateTimeType::class,
				[
					'label'  => 'Departure Arrival',
					'html5'  => true,
					'attr' => [
						'placeholder' => 'Arrival date',
						'class'       => 'boldText',
					],
				]
			)
			
			->add('isActiveTrip', CheckboxType::class,
				[
					'label' => 'is Active',
					'data' => true,
				])

		;
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			[
				'data_class' => Trips::class,
				'attr'       => [
//					'novalidate' => 'novalidate',
					'class'      => 'form-horizontal',
				],
			]
		);
		
	}
}
