<?php

namespace App\Form;

use App\Entity\UserTrips;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class UserTripsType extends AbstractType
{
	
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$builder
			->add(
				'utTripsLink',
				ChoiceType::class,
				[
					'choices' => $options['choice_options'],
				]
			);
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			[
				'data_class'     => UserTrips::class,
				'choice_options' => null,
				'attr'           => [
					'class' => 'form-horizontal',
				],
			]
		);
		
	}
}
