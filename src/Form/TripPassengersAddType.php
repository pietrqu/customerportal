<?php

namespace App\Form;

use App\Entity\TripPassengers;
use App\Entity\UserPassengers;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class TripPassengersAddType extends AbstractType
{
	
	/**
	 * @var integer
	 */
	private $userId;
	
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['choice_options'];
		
		$builder
			->add(
				'tpUserPassengersLink',
				EntityType::class,
				[
					'label'         => false,
					'attr'          => ['placeholder' => 'the name of the trip'],
					'choice_label'  => 'fullName',
					'expanded'      => true,
					'multiple'      => true,
					'class'         => UserPassengers::class,
					'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
						return $er->createQueryBuilder('u')->andWhere('u.userIdLink = :param1')
							->andWhere('u.isActive = true')
							->setParameter('param1', $this->userId);
					},
				
				]
			);
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			[
				'data_class'     => TripPassengers::class,
				'choice_options' => null,
			]
		);
		
	}
}
