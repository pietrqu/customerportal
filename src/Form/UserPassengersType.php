<?php

namespace App\Form;

use App\Entity\UserPassengers;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserPassengersType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			//also can be as entity list (really entited not needed here)
			->add(
				'pTitle',
				ChoiceType::class,
				[
					
					'label' => 'Honorific',
					
					'choices' => [
						'Mr'   => 'Mr',
						'Ms'   => 'Ms',
						'Mrs'  => 'Mrs',
						'Miss' => 'Miss',
					],
				]
			)
			->add(
				'pName',
				TextType::class,
				[
					'label' => false,
					'attr' => ['placeholder' => 'Name']
				]
			)
			->add(
				'pSurname',
				TextType::class,
				[
					'label' => false,
					'attr' => ['placeholder' => 'Surname']
				]
			)
			
			->add(
				'passport',
				TextType::class,
				[
					'label' => false,
					'attr' => ['placeholder' => 'Passport']
				]
			)
			->add('isActive', CheckboxType::class,
				[
					'required' => true,
					'data' => true,
					
				])
		
		;
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			[
				'data_class' => UserPassengers::class,
				'attr'       => [
					'novalidate' => 'novalidate',
					'class'      => 'form-horizontal',
				],
			]
		);
		
	}
}
