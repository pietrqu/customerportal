<?php

namespace App\Controller;

use App\Entity\TripPassengers;
use App\Entity\Trips;
use App\Entity\UserPassengers;
use App\Entity\UserTrips;
use App\Form\TripPassengersAddType;
use App\Form\TripsType;
use App\Form\UserBasicInfoType;
use App\Form\UserPassengersType;
use App\Form\UserTripsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends AbstractController
{
	
	/**
	 * @Route("/", name="index")
	 */
	public function indexAction()
	{
		return $this->render(
			'index.html.twig'
		);
	}
	
	/**
	 * @Route("dashboard", name="user_dashboard_Index", methods={"POST", "GET"})
	 * @param Request|null $request
	 * @return Response
	 */
	
	public function dashboardAction(Request $request = null)
	{
		if ($this->preAuth() === true) {
			
			$user = $this->getUser();
			$em = $this->preManager();
			
			$passengersListAr = null;
			$passengersListAr = $em->getRepository(UserPassengers::class)
				->findBy(
					[
						'userIdLink' => $user->getId(),
					]
				);
			
			$tripsListAr = null;
			$tripsListAr = $em->getRepository(Trips::class)
				->findAll();
			
			
			//by EAGER in relation UserTrips, this simple query gets trips and passengers
			$userTrips = null;
			$userTrips = $em->getRepository(UserTrips::class)
				->findUserTrips($user->getId());
			
			
			$userForm = $this->createForm(UserBasicInfoType::class, $user);
			$userForm->handleRequest($request);
			
			//simple form objects, there is no need to repeat "form processing" code
			$this->preHandleSimpleFormRequest($userForm, 'user_dashboard_Index');
			
			$passengers = null;
			$trips = null;
			
			
			return $this->render(
				'dashboard.html.twig',
				[
					'formDa'     => $userForm->createView(),
					'passengers' => $passengersListAr,
					'trips'      => $tripsListAr,
					'userTrips'  => $userTrips,
				]
			);
			
		}
		
		$this->addFlash('warning', 'Acces denied');
		
		return new RedirectResponse($this->generateUrl('index'));
	}
	
	/**
	 * @param Request $request
	 * @return RedirectResponse|Response
	 * @Route("passenger-add", name="user_dashboard_PassengerAdd", methods={"POST", "GET"})
	 */
	public function addPassenger(Request $request)
	{
		if ($this->preAuth() === true) {
			
			$user = $this->getUser();
			$passengerForm = $this->createForm(UserPassengersType::class);
			$passengerForm->handleRequest($request);
			
			if ($passengerForm->isSubmitted() && $passengerForm->isValid()) {
				$formPostData = $passengerForm->getData();
				$formPostData->setUserIdLink($user);
				
				
				$this->preSaveObjectIntoDatabase($formPostData);
				
				return $this->redirectToRoute('user_dashboard_Index');
			}
			
			return $this->render(
				'user/passenger.add.html.twig',
				[
					'formPa' => $passengerForm->createView(),
				]
			);
		}
		
		$this->addFlash('warning', 'Acces denied');
		
		return new RedirectResponse($this->generateUrl('index'));
	}
	
	/**
	 * @param Request $request
	 * @return RedirectResponse|Response
	 * @Route("trip-add", name="user_dashboard_TripAdd", methods={"POST", "GET"})
	 */
	public function addTrip(Request $request)
	{
		if ($this->preAuth() === true) {
			
			$tripForm = $this->createForm(TripsType::class);
			$tripForm->handleRequest($request);
			
			if ($tripForm->isSubmitted() && $tripForm->isValid()) {
				$formPostData = $tripForm->getData();
				
				$this->preSaveObjectIntoDatabase($formPostData);
				
				return $this->redirectToRoute('user_dashboard_Index');
			}
			
			return $this->render(
				'user/trip.add.html.twig',
				[
					'formTrip' => $tripForm->createView(),
				]
			);
		}
		
		$this->addFlash('warning', 'Acces denied');
		
		return new RedirectResponse($this->generateUrl('index'));
	}
	
	
	/**
	 * @param Request $request
	 * @return RedirectResponse
	 * @Route("journey-add", name="user_dshboard_JourneyAdd", methods={"POST", "GET"})
	 *
	 */
	public function addTripToUserTrips(Request $request = null)
	{
		
		$userId = $this->getUser()->getId();
		
		$em = $this->preManager();
		$tripsAr = $em->getRepository(Trips::class)
			->findNotSelectedTrips($userId);
		
		
		$passChoices = array();
		
		foreach ($tripsAr as $key => $items) {
			$passChoices['Trip'.'#'.$key] =
				[
					$items->getTrName() => $items->getId(),
				];
		}
		
		$addJourney = $this->createForm(
			UserTripsType::class,
			null,
			[
				'choice_options' => $passChoices,
			]
		);
		$addJourney->handleRequest($request);
		
		if ($addJourney->isSubmitted() && $addJourney->isValid()) {
			
			$formPostData = $addJourney->getData();
			$getObject = $em->getRepository(Trips::class)
				->findOneBy(
					[
						'id' => $formPostData->getUtTripsLink(),
					]
				);
			
			$formPostData->setUtTripsLink($getObject);
			$formPostData->setTrOwnerId($userId);
			
			$this->preSaveObjectIntoDatabase($formPostData);
			
			return $this->redirectToRoute('user_dashboard_Index');
		}
		
		return $this->render(
			'user/trip.add.html.twig',
			[
				'formTrip' => $addJourney->createView(),
			]
		);
		
	}
	
	/**
	 * @param Request|null $request
	 * @param $idTrip
	 * @return RedirectResponse
	 * @Route("passengers-add/{idTrip}", name="user_dashboard_userPassengersAdd")
	 */
	public function addPassengersAction(Request $request = null, $idTrip)
	{
		if ($this->preAuth() === true) {
			
			
			$user = $this->getUser();
			
			$passengerForm = $this->createForm(
				TripPassengersAddType::class,
				null,
				[
					'choice_options' => $user->getId(),
				]
			);
			
			$passengerForm->handleRequest($request);
			
			if ($passengerForm->isSubmitted() && $passengerForm->isValid()) {
				
				$formPostData = $passengerForm->getData();
				$em = $this->preManager();
				
				$tripsUserObject = $em->getRepository(UserTrips::class)
					->findOneBy(
						[
							'id' => $idTrip,
						]
					);
				
				
				if ($formPostData->gettpUserPassengersLink()->isEmpty()) {
					$this->addFlash('danger', 'Select at least one passanger or back to dashboard');
					
					return $this->redirectToRoute('user_dashboard_userPassengersAdd', ['idTrip' => $idTrip]);
				} else {
					
					foreach ($formPostData->gettpUserPassengersLink() as $userPassengers) {
						
						$newPassengerAddToTrip = new TripPassengers();
						$newPassengerAddToTrip->setTpUserTripsLink($tripsUserObject);
						$newPassengerAddToTrip->setTpUserPassengersLink($userPassengers);
						
						
						$checkPassenger = $this->preCheckIfPassangerIsOnTripList($userPassengers, $idTrip, $newPassengerAddToTrip);
						
						
						if ($checkPassenger == true) {
							$this->preSaveObjectIntoDatabase($newPassengerAddToTrip);
							$this->addFlash(
								'success',
								'Succesfully Added to Journey'
								.' '.$newPassengerAddToTrip->getTpUserPassengersLink()->getFullName()
							);
						}
						
					}
					
					return $this->redirectToRoute('user_dashboard_Index');
					
				}
			}
			
			return $this->render(
				'user/passenger.add.to.trip.html.twig',
				[
					'formPuAdd' => $passengerForm->createView(),
				]
			);
			
			return $this->redirectToRoute('user_dashboard_Index');
			
		}
		
		$this->addFlash('warning', 'Acces denied #H');
		
		return new RedirectResponse($this->generateUrl('index'));
	}
	
	
	/**
	 * @param $userId
	 * @return RedirectResponse
	 * @Route("disable/{userId}", name="user_dahboard_disableUser")
	 */
	public function disabelUserAction($userId)
	{
		
		
		$action = $this->preUserListEnableOrDisablePassanger($userId, false);
		
		if($action == true){
			$this->addFlash('success','User is Disabled');
		}
		
		return $this->redirectToRoute('user_dashboard_Index');
		
	}
	
	
	/**
	 * @param $userId
	 * @return RedirectResponse
	 * @Route("enable/{userId}", name="user_dahboard_enableUser")
	 */
	public function enablelUserAction($userId)
	{
		
		$action = $this->preUserListEnableOrDisablePassanger($userId, true);
		
		if($action == true){
			$this->addFlash('success','User is Enabled');
		}
		
		return $this->redirectToRoute('user_dashboard_Index');
		
	}
	
	
	
	// ===================
	//all helpers can be moved into separate class if needed, used as service etc, app is too small for that by now
	
	/**
	 * @Route("/profile", name="user_profile" ,methods={"GET", "POST"})
	 */
	public function profile()
	{
		return $this->render('user/profile.html.twig');
	}
	
	
	/**Helper for getting Doctrine EM
	 *
	 * @return \Doctrine\Common\Persistence\ObjectManager
	 */
	private function preManager()
	{
		return $this->getDoctrine()->getManager();
	}
	
	/**
	 * Helper for checking authorisation in security routes PP (For ADMINS only for examle)
	 *
	 * @return bool|\Symfony\Component\HttpFoundation\RedirectResponse
	 */
	private function preAuth()
	{
		$authChecker = $this->get('security.authorization_checker');
		
		if ($authChecker->isGranted('ROLE_ADMIN') || $authChecker->isGranted('ROLE_USER')) {
			return true;
		} else {
			$this->addFlash('warning', 'Access denied');
			
			return $this->redirect($this->generateUrl('index'));
		}
		
		return false;
	}
	
	
	/**
	 * Helper - save object into database in form PP
	 *
	 * @param $object
	 */
	
	private function preSaveObjectIntoDatabase($object)
	{
		$em = $this->preManager();
		$em->persist($object);
		$em->flush($object);
		
		return;
	}
	
	/**
	 * Helper - process form request as full object
	 *
	 * @param $formObject
	 * @param $redirectRoute
	 * @return RedirectResponse
	 */
	private function preHandleSimpleFormRequest($formObject, $redirectRoute)
	{
		if ($formObject->isSubmitted() && $formObject->isValid()) {
			
			$formPostData = $formObject->getData();
			$this->preSaveObjectIntoDatabase($formPostData);
			
			return $this->redirectToRoute($redirectRoute);
		}
	}
	
	private function preCheckIfPassangerIsOnTripList($userPassengers, $idTrip, TripPassengers $passengers)
	{
		$userId = $userPassengers->getId();
		$tripId = $idTrip;
		
		$em = $this->preManager();
		
		$status = '';
		$status = $em->getRepository(TripPassengers::class)
			->findBy(
				[
					'tpUserPassengersLink' => $userId,
					'tpUserTripsLink'      => $tripId,
				
				]
			);
		
		if ($status == !null) {
			$this->addFlash(
				'warning',
				'Passenger is already on list, not added'.' '.
				$passengers->getTpUserPassengersLink()->getFullName()
			);
			
			return false;
		}
		
		return true;
		
	}
	
	private function preUserListEnableOrDisablePassanger($userId, $action)
	{
		$currUser = $this->getUser();
		$em = $this->preManager();
		
		$findPassanger = $em->getRepository(UserPassengers::class)
			->findOneBy(
				[
					'id'         => $userId,
					'userIdLink' => $currUser,
				]
			);
		
		if ($findPassanger !== null) {
			
			$findPassanger->setIsActive($action);
			$em->persist($findPassanger);
			$em->flush($findPassanger);
			
			return true;
		}
		
		return false;
	 }
}