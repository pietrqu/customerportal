<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class TripsRepository extends EntityRepository
{
	
	/**Returns only not selected trips for user
	 *
	 * @param $user
	 * @return mixed
	 */
	public function findNotSelectedTrips($user)
	{
		$query = $this->getEntityManager()
			->createQuery(
				'SELECT t FROM App:Trips t
							WHERE
							t.isActiveTrip = true
							AND t.id NOT IN (
								SELECT IDENTITY(uf.utTripsLink)
								FROM  App:UserTrips uf, App:Trips ut
								WHERE uf.utTripsLink = ut.id
								AND uf.trOwnerId = ?1
							)
							ORDER BY t.trName ASC
					'
			);
		
		$query->setParameters(
			[
				1 => $user,
			]
		);
		
		return $query->getResult();
	}
	
	/**
	 * quick sample code to test is sub query or part of query just working
	 *
	 * @param $user
	 * @return mixed
	 */
	public function findTest($user)
	{
		$query = $this->getEntityManager()
			->createQuery(
				'
					SELECT IDENTITY(uf.utTripsLink)
					FROM  App:UserTrips uf, App:Trips ut
					WHERE uf.utTripsLink = ut.id
					AND uf.trOwnerId = ?1
					'
			);
		
		$query->setParameters(
			[
				1 => $user,
			]
		);
		
		return $query->getResult();
	}
}
