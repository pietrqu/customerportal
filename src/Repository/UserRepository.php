<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
	/**
	 * UserRepository constructor.
	 * @param RegistryInterface $registry
	 */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }
	
	/**
	 * @param string $username
	 * @return mixed|\Symfony\Component\Security\Core\User\UserInterface|null
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
	
	/**
	 * @return mixed
	 */
    public function findAllUsers(){
    	
    	$query = $this->getEntityManager()
			->createQuery(
				'
				SELECT u
				FROM App:User u
				ORDER BY u.username
				'
			)
			->setMaxResults(5);
    	
    	return $query->getResult();
	}
	
}
