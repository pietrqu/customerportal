<?php


namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class UserPassenegersRepository extends EntityRepository
{
	
	
	
	/**
	 * @param $user
	 * @return QueryBuilder
	 */
	public function findOwnedBy($user){
		return $this->findUserPassengers($user)
			;
	}
	
	
	public function findUserPassengers($user)
	{
		return $this->createQueryBuilder('u')
			->where('u.userIdLink = :userQ')
			->setParameter('userQ', $user)
			;
//			->getQuery()
//		->getArrayResult();
			//->getResult();
	}
	
}