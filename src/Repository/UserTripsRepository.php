<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class UserTripsRepository extends EntityRepository
{
	/**
	 * Retrive user trips and passengers by EAGER relation
	 *
	 * @param $userId
	 * @return mixed
	 */
	public function findUserTrips($userId)
	{
		$query = $this->getEntityManager()
			->createQuery(
				'SELECT t
						FROM App:UserTrips t
						WHERE
							t.trOwnerId = ?1
						ORDER BY t.id ASC
				'
			);
		
		$query->setParameters(
			[
				1 => $userId,
			]
		);
		
		return $query->getResult();
	}
	
}
    