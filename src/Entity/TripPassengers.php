<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="trip_passengers")
 */
class TripPassengers
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\UserTrips", inversedBy="utTripPassengersList", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 */
	private $tpUserTripsLink;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\UserPassengers", inversedBy="id", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 */
	private $tpUserPassengersLink;
	
	/**
	 * @return mixed
	 */
	public function getTpUserTripsLink()
	{
		return $this->tpUserTripsLink;
	}
	
	/**
	 * @param mixed $tpUserTripsLink
	 */
	public function setTpUserTripsLink(UserTrips $tpUserTripsLink): void
	{
		$this->tpUserTripsLink = $tpUserTripsLink;
	}
	
	/**
	 * @return mixed
	 */
	public function getTpUserPassengersLink()
	{
		return $this->tpUserPassengersLink;
	}
	
	/**
	 * @param mixed
	 */
	public function setTpUserPassengersLink( $tpUserPassengersLink): void
	{
		$this->tpUserPassengersLink = $tpUserPassengersLink;
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	

	
}
    