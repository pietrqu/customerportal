<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserTripsRepository")
 * @ORM\Table(name="user_trips")
 */
class UserTrips
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Trips", inversedBy="trUserTripsLink", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 */
	private $utTripsLink;

	
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\TripPassengers", mappedBy="tpUserTripsLink", cascade={"persist"}, fetch="EAGER")
	 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 */
	private $utTripPassengersList;


	public function __construct()
	{
		$this->utTripPassengersList = new ArrayCollection();
	}

	/**
	 * @ORM\Column(type="integer")
	 */
	private $trOwnerId;
	
	
	/**
	 * @return mixed
	 */
	public function getUtTripsLink()
	{
		return $this->utTripsLink;
	}
	
	/**
	 * @param mixed $utTripsLink
	 */
	public function setUtTripsLink($utTripsLink): void
	{
		$this->utTripsLink = $utTripsLink;
	}
	
	/**
	 * @return mixed
	 */
	public function getUtTripPassengersList()
	{
		return $this->utTripPassengersList;
	}
	
	/**
	 * @param TripPassengers $utTripPassengersList
	 */
	public function setUtTripPassengersList(TripPassengers $utTripPassengersList): void
	{
		$this->utTripPassengersList = $utTripPassengersList;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrOwnerId()
	{
		return $this->trOwnerId;
	}
	
	/**
	 * @param mixed $trOwnerId
	 */
	public function setTrOwnerId($trOwnerId): void
	{
		$this->trOwnerId = $trOwnerId;
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	
}
    