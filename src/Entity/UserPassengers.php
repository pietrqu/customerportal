<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPassenegersRepository")
 * @ORM\Table(name="user_passengers")
 */
class UserPassengers
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPassengersLink")
	 */
	private $userIdLink;
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $pTitle;
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $pName;
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $pSurname;
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $passport;
	
	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", options={"default":1})
	 */
	private $isActive = 0;
	
	/**
	 * @var string
	 */
	private $fullName;
	
	
	/**
	 * @return mixed
	 */
	public function getUserIdLink()
	{
		return $this->userIdLink;
	}

	/**
	 * @param mixed $userIdLink
	 */
	public function setUserIdLink(User $userIdLink): void
	{
		$this->userIdLink = $userIdLink;
	}
	
	/**
	 * @return mixed
	 */
	public function getPTitle()
	{
		return $this->pTitle;
	}
	
	/**
	 * @param mixed $pTitle
	 */
	public function setPTitle($pTitle): void
	{
		$this->pTitle = $pTitle;
	}
	
	/**
	 * @return mixed
	 */
	public function getPName()
	{
		return $this->pName;
	}
	
	/**
	 * @param mixed $pName
	 */
	public function setPName($pName): void
	{
		$this->pName = $pName;
	}
	
	/**
	 * @return mixed
	 */
	public function getPSurname()
	{
		return $this->pSurname;
	}
	
	/**
	 * @param mixed $pSurname
	 */
	public function setPSurname($pSurname): void
	{
		$this->pSurname = $pSurname;
	}
	
	/**
	 * @return mixed
	 */
	public function getPassport()
	{
		return $this->passport;
	}
	
	/**
	 * @param mixed $passport
	 */
	public function setPassport($passport): void
	{
		$this->passport = $passport;
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @return bool
	 */
	public function isActive(): bool
	{
		
		return $this->isActive;
	}
	
	/**
	 * @return string
	 */
	public function getFullName(): string
	{
		$data = $this->fullName = $this->pName.' '.$this->pSurname;
		return $this->fullName = $data;
	}
	
	
	/**
	 * @param bool $isActive
	 */
	public function setIsActive(bool $isActive): void
	{
		
		$this->isActive = $isActive;
	}
	
	

	
}
    