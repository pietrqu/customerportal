<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TripsRepository")
 * @ORM\Table(name="trips")
 */
class Trips
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $trName;
	
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $trLocationFrom;
	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotNull()
	 */
	private $trLocationTo;
	/**
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull()
	 * @Assert\NotBlank()
	 */
	private $trDeparture;
	/**
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull()
	 */
	private $trArrival;
	
	
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\UserTrips", mappedBy="utTripsLink", cascade={"persist"})
	 * * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
	 */
	private $trUserTripsLink;
	
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isActiveTrip;
	

	/**
	 * @return mixed
	 */
	public function getTrName()
	{
		return $this->trName;
	}
	
	/**
	 * @param mixed $trName
	 */
	public function setTrName($trName): void
	{
		$this->trName = $trName;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrLocationFrom()
	{
		return $this->trLocationFrom;
	}
	
	/**
	 * @param mixed $trLocationFrom
	 */
	public function setTrLocationFrom($trLocationFrom): void
	{
		$this->trLocationFrom = $trLocationFrom;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrLocationTo()
	{
		return $this->trLocationTo;
	}
	
	/**
	 * @param mixed $trLocationTo
	 */
	public function setTrLocationTo($trLocationTo): void
	{
		$this->trLocationTo = $trLocationTo;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrDeparture()
	{
		return $this->trDeparture;
	}
	
	/**
	 * @param mixed $trDeparture
	 */
	public function setTrDeparture(\DateTime $trDeparture): void
	{
		$this->trDeparture = $trDeparture;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrArrival()
	{
		return $this->trArrival;
	}
	
	/**
	 * @param mixed $trArrival
	 */
	public function setTrArrival(\DateTime $trArrival): void
	{
		$this->trArrival = $trArrival;
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @return mixed
	 */
	public function getIsActiveTrip()
	{
		return $this->isActiveTrip;
	}
	
	/**
	 * @param mixed $isActiveTrip
	 */
	public function setIsActiveTrip($isActiveTrip): void
	{
		$this->isActiveTrip = $isActiveTrip;
	}
	
	/**
	 *
	 */
	public function __toString()
	{
		return (string)$this->id;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrUserTripsLink()
	{
		return $this->trUserTripsLink;
	}
	
	/**
	 * @param mixed $trUserTripsLink
	 */
	public function setTrUserTripsLink(UserTrips $trUserTripsLink): void
	{
		$this->trUserTripsLink = $trUserTripsLink;
	}
	
	
}
    