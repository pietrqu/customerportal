<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="email", message="This email is already taken.")
 * @UniqueEntity(fields="username", message="This username is already taken.")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, \Serializable
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", unique=true)
	 * @Assert\NotBlank()
	 */
	private $username;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", unique=true)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank()
	 */
	private $password;
	
	/**
	 * @var array
	 *
	 * @ORM\Column(type="json")
	 */
	private $roles = [];
	
	/**
	 * @var \Datetime
	 *
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $last_login;
	
	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $created_at;
	
	
	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $addrStreet;
	
	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $addrNumber;
	
	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $addrPostCode;
	
	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $addrCity;
	
	
	/**
	 * @ORM\Column(name="addr_country", type="string", nullable=true)
	 */
	private $addrCountry;
	
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\UserPassengers", mappedBy="userIdLink", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
	 */
	private $userPassengersLink;
	
	
//	/**
//	 * @ORM\OneToMany(targetEntity="App\Entity\UserTrips", mappedBy="utUserLink", cascade={"persist", "remove"})
//	 * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
//	 */
//	private $userTripsLink;
	
	
	
	public function __construct()
	{
		$this->userPassengersLink = new ArrayCollection();
//		$this->userTripsLink = new ArrayCollection();
	}
	
	public function getId(): int
	{
		return $this->id;
	}
	
	public function setFullName(string $fullName): void
	{
		$this->fullName = $fullName;
	}
	
	public function getFullName(): ?string
	{
		return $this->fullName;
	}
	
	public function getUsername(): ?string
	{
		return $this->username;
	}
	
	public function setUsername(string $username): void
	{
		$this->username = $username;
	}
	
	public function getEmail(): ?string
	{
		return $this->email;
	}
	
	public function setEmail(string $email): void
	{
		$this->email = $email;
	}
	
	public function getPassword(): ?string
	{
		return $this->password;
	}
	
	public function setPassword(string $password): void
	{
		$this->password = $password;
	}
	
	public function getRoles(): array
	{
		$roles = $this->roles;
		
		if (empty($roles)) {
			$roles[] = 'ROLE_USER';
		}
		
		return array_unique($roles);
	}
	
	public function setRoles(array $roles): void
	{
		$this->roles = $roles;
	}
	
	public function getSalt(): ?string
	{
		return null;
	}
	
	public function eraseCredentials(): void
	{
		//
	}
	
	public function serialize(): string
	{
		return serialize([$this->id, $this->username, $this->password]);
	}
	
	public function unserialize($serialized): void
	{
		[$this->id, $this->username, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
	}
	
	public function getLastLogin()
	{
		return $this->last_login;
	}
	
	public function setLastLogin($last_login): void
	{
		$this->last_login = $last_login;
	}
	
	public function getCreatedAt(): \DateTime
	{
		return $this->created_at;
	}
	
	public function setCreatedAt(\DateTime $created_at = null): void
	{
		$this->created_at = $created_at;
	}
	
	/**
	 * @ORM\PrePersist
	 */
	public function onPrePersist()
	{
		$this->created_at = new \DateTime();
	}
	
	/**
	 * @return mixed
	 */
	public function getAddrStreet()
	{
		return $this->addrStreet;
	}
	
	/**
	 * @param mixed $addrStreet
	 */
	public function setAddrStreet($addrStreet): void
	{
		$this->addrStreet = $addrStreet;
	}
	
	/**
	 * @return mixed
	 */
	public function getAddrNumber()
	{
		return $this->addrNumber;
	}
	
	/**
	 * @param mixed $addrNumber
	 */
	public function setAddrNumber($addrNumber): void
	{
		$this->addrNumber = $addrNumber;
	}
	
	/**
	 * @return mixed
	 */
	public function getAddrPostCode()
	{
		return $this->addrPostCode;
	}
	
	/**
	 * @param mixed $addrPostCode
	 */
	public function setAddrPostCode($addrPostCode): void
	{
		$this->addrPostCode = $addrPostCode;
	}
	
	/**
	 * @return mixed
	 */
	public function getAddrCity()
	{
		return $this->addrCity;
	}
	
	/**
	 * @param mixed $addrCity
	 */
	public function setAddrCity($addrCity): void
	{
		$this->addrCity = $addrCity;
	}
	
	/**
	 * @return mixed
	 */
	public function getAddrCountry()
	{
		return $this->addrCountry;
	}
	
	/**
	 * @param mixed $addrCountry
	 */
	public function setAddrCountry($addrCountry): void
	{
		$this->addrCountry = $addrCountry;
	}
	
	/**
	 * @return mixed
	 */
	public function getUserPassengersLink()
	{
		return $this->userPassengersLink;
	}
	
	/**
	 * @param mixed $userPassengersLink
	 */
	public function setUserPassengersLink($userPassengersLink): void
	{
		$this->userPassengersLink = $userPassengersLink;
	}
	
	
}