/*
Navicat MySQL Data Transfer

Source Server         : mySQL_VPS_MYSQL
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : customerportal

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-10 18:00:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migration_versions
-- ----------------------------
DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migration_versions
-- ----------------------------
INSERT INTO `migration_versions` VALUES ('20190708172111', '2019-07-08 17:21:14');
INSERT INTO `migration_versions` VALUES ('20190708213514', '2019-07-08 21:35:21');
INSERT INTO `migration_versions` VALUES ('20190709054623', '2019-07-09 05:46:27');
INSERT INTO `migration_versions` VALUES ('20190709060312', '2019-07-09 06:03:15');
INSERT INTO `migration_versions` VALUES ('20190709060500', '2019-07-09 06:05:03');
INSERT INTO `migration_versions` VALUES ('20190709060602', '2019-07-09 06:06:04');
INSERT INTO `migration_versions` VALUES ('20190709061246', '2019-07-09 06:12:51');
INSERT INTO `migration_versions` VALUES ('20190709061848', '2019-07-09 06:18:50');
INSERT INTO `migration_versions` VALUES ('20190709062637', '2019-07-09 06:26:41');
INSERT INTO `migration_versions` VALUES ('20190709062729', '2019-07-09 06:27:31');
INSERT INTO `migration_versions` VALUES ('20190709064204', '2019-07-09 06:42:07');
INSERT INTO `migration_versions` VALUES ('20190709163218', '2019-07-09 16:32:23');
INSERT INTO `migration_versions` VALUES ('20190709224249', '2019-07-09 22:42:53');
INSERT INTO `migration_versions` VALUES ('20190709224738', '2019-07-09 22:47:44');
INSERT INTO `migration_versions` VALUES ('20190709225217', '2019-07-09 22:52:24');
INSERT INTO `migration_versions` VALUES ('20190709225438', '2019-07-09 22:54:44');
INSERT INTO `migration_versions` VALUES ('20190709230212', '2019-07-09 23:02:17');
INSERT INTO `migration_versions` VALUES ('20190709230522', '2019-07-09 23:05:27');
INSERT INTO `migration_versions` VALUES ('20190710073623', '2019-07-10 07:36:28');

-- ----------------------------
-- Table structure for trip_passengers
-- ----------------------------
DROP TABLE IF EXISTS `trip_passengers`;
CREATE TABLE `trip_passengers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tp_user_trips_link_id` int(11) DEFAULT NULL,
  `tp_user_passengers_link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1645559C35CA6297` (`tp_user_trips_link_id`),
  KEY `IDX_1645559CEEB67442` (`tp_user_passengers_link_id`),
  CONSTRAINT `FK_1645559C35CA6297` FOREIGN KEY (`tp_user_trips_link_id`) REFERENCES `user_trips` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_1645559CEEB67442` FOREIGN KEY (`tp_user_passengers_link_id`) REFERENCES `user_passengers` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of trip_passengers
-- ----------------------------
INSERT INTO `trip_passengers` VALUES ('103', '1', '1');
INSERT INTO `trip_passengers` VALUES ('104', '1', '2');
INSERT INTO `trip_passengers` VALUES ('105', '1', '3');
INSERT INTO `trip_passengers` VALUES ('106', '1', '4');

-- ----------------------------
-- Table structure for trips
-- ----------------------------
DROP TABLE IF EXISTS `trips`;
CREATE TABLE `trips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tr_location_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tr_location_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tr_departure` datetime NOT NULL,
  `tr_arrival` datetime NOT NULL,
  `is_active_trip` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of trips
-- ----------------------------
INSERT INTO `trips` VALUES ('1', 'Trip To Congo', 'FLA', 'EAX', '2019-01-01 20:51:00', '2019-02-03 00:00:00', '1');
INSERT INTO `trips` VALUES ('2', 'WĄCHOCK', 'POL', 'BRA', '2020-03-03 12:00:00', '2020-03-04 18:00:00', '1');
INSERT INTO `trips` VALUES ('3', 'ALCATRAZ', 'NDL', 'USA', '2020-01-01 00:00:00', '2020-01-05 12:00:00', '1');
INSERT INTO `trips` VALUES ('4', 'WŁADYWOSTOK', 'BEL', 'RUS', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '1');
INSERT INTO `trips` VALUES ('5', 'Burgundia', 'AFX', 'DAL', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `addr_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr_post_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addr_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('52', 'orville.yost', 'hgutkowski@example.com', '$2y$13$BCFB/7P8fOHR4jNmSq6XsuYshmjPPHyzE2FuyoLSNg0PtXjIZc7.u', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:34', 'REYMONTA', '17', '60-999', 'WĄCHOCK', 'NEW ZELAND');
INSERT INTO `user` VALUES ('53', 'kutch.abbie', 'terry.leatha@example.com', '$2y$13$QE7wGuZr65AQEwJaBFoB4.VvrymZFCnWXK4QEc9p0tAndFmwk9dfu', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:35', 'ŻEROMSKIEGO', '17', '60-999', 'LUBLIN', 'BRASIL');
INSERT INTO `user` VALUES ('54', 'brianne91', 'connelly.gilberto@example.org', '$2y$13$gWX8QhnkJ0cMgx.vrgvcfeCCDs2X9C497AZUwzDdCtdGDDfMf.tfO', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:35', 'REYMONTA', '17', '60-999', 'BIAŁYSTOK', 'ENGLAND');
INSERT INTO `user` VALUES ('55', 'huel.ron', 'immanuel.labadie@example.net', '$2y$13$TOepovo5yCdFchF2f6xBEOhdS43/LKrIQVGk0Hl21YErsHR4/hE16', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:36', 'REYMONTA', '17', '60-999', 'BADMINKTON', 'NEW ZELAND');
INSERT INTO `user` VALUES ('56', 'kenya43', 'wolf.jessy@example.net', '$2y$13$HG0H63UccGixytGAOMtccuBuPDmlqrnS5CQqc5F2FBvE214iuPi5S', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:37', 'MICKIEWICZA', '17', '60-999', 'LONDON', 'NEW ZELAND');
INSERT INTO `user` VALUES ('57', 'janessa02', 'hackett.salma@example.net', '$2y$13$FYJX0T2LZ9vCCqDqcmih0uBIFDddbUSW/UFwN4ClWMJ5pWpInekmW', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:37', 'SIENKIEWICZA', '17', '60-999', 'LONDON', 'USA');
INSERT INTO `user` VALUES ('58', 'okon.joana', 'brionna.parisian@example.net', '$2y$13$MprMnR4Z1kJJd2QExfs59u9scFDSTKPtpafGOqTY6bjz/QJ4z6F8.', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:38', 'ŻEROMSKIEGO', '17', '60-999', 'LONDON', 'POLAND');
INSERT INTO `user` VALUES ('59', 'price.cora', 'sstark@example.net', '$2y$13$P286UMWWL09NrrjCrC7JEeHByLFN9mCYot19EynS2wyinVd/W9JzK', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:38', 'SIENKIEWICZA', '17', '60-999', 'RAWICZ', 'JAPAN');
INSERT INTO `user` VALUES ('60', 'abigayle77', 'hfadel@example.net', '$2y$13$R.KruTS/C4uZ3bllnWhcoO71vayG8luTfw4JBk1Syb5cUo4nl3ZQG', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:39', 'ŻEROMSKIEGO', '17', '60-999', 'WĄCHOCK', 'NEW ZELAND');
INSERT INTO `user` VALUES ('61', 'gibson.maryjane', 'hsporer@example.com', '$2y$13$75X1rWT5snErrirkPnnGQ.NGxbI6szkDTBt49x5V24J2tyqx3KtDe', '[\"ROLE_USER\"]', null, '2019-07-08 19:45:40', 'ORZESZKOWEJ', '17', '60-999', 'WĄCHOCK', 'USA');
INSERT INTO `user` VALUES ('62', 'Piotr Potrawiak', 'piotr.potrawiak@gmail.com', '$2y$13$uTvok3kp8dQAw2CCl79M6.VP64h16dCbUoBMLlaPdRct4icF9CFZK', '[\"ROLE_USER\"]', '2019-07-10 12:24:03', '2019-07-08 19:53:27', 'Baker Street 21B', null, null, 'London', 'England');
INSERT INTO `user` VALUES ('63', 'dd', 'd@ed.de', '$2y$13$HCgN42CulC9/VjZGFQ1JW.vLL7E9NxpGXGBYzJ9tqutg/cua1Ayd2', '[\"ROLE_USER\"]', '2019-07-10 15:24:19', '2019-07-08 21:07:48', 'ss', null, null, null, 'PANAMA');

-- ----------------------------
-- Table structure for user_passengers
-- ----------------------------
DROP TABLE IF EXISTS `user_passengers`;
CREATE TABLE `user_passengers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `user_id_link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_44304866D45E6DED` (`user_id_link_id`),
  CONSTRAINT `FK_44304866D45E6DED` FOREIGN KEY (`user_id_link_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_passengers
-- ----------------------------
INSERT INTO `user_passengers` VALUES ('1', 'Mr', 'Piotr', 'Potrawiak', '1234567', '1', '63');
INSERT INTO `user_passengers` VALUES ('2', 'Mrs', 'Angelika', 'Kozidrak', '098237482903AGHGD', '1', '63');
INSERT INTO `user_passengers` VALUES ('3', 'Miss', 'Tora', 'Bora', '787VcxIU', '1', '63');
INSERT INTO `user_passengers` VALUES ('4', 'Miss', 'Nutella', 'KFC', '8887263hjgjh8', '1', '63');
INSERT INTO `user_passengers` VALUES ('5', 'Miss', 'Jolanta', 'Abrakadabra', '9879483nmnm', '1', '62');
INSERT INTO `user_passengers` VALUES ('6', 'Ms', 'Żaneta', 'ŹĆŚBŁĄKIEWICZ', '9837894n,mcvn', '1', '62');

-- ----------------------------
-- Table structure for user_trips
-- ----------------------------
DROP TABLE IF EXISTS `user_trips`;
CREATE TABLE `user_trips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tr_owner_id` int(11) NOT NULL,
  `ut_trips_link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_48B18B82AE3702D7` (`ut_trips_link_id`),
  CONSTRAINT `FK_48B18B82AE3702D7` FOREIGN KEY (`ut_trips_link_id`) REFERENCES `trips` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_trips
-- ----------------------------
INSERT INTO `user_trips` VALUES ('1', '63', '1');
INSERT INTO `user_trips` VALUES ('2', '63', '3');
INSERT INTO `user_trips` VALUES ('3', '63', '2');
INSERT INTO `user_trips` VALUES ('5', '62', '2');
INSERT INTO `user_trips` VALUES ('6', '62', '1');
INSERT INTO `user_trips` VALUES ('7', '62', '3');
